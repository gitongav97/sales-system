"""utensils URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from core.views import *
from django.contrib.auth import views as auth_views
from core.utils import *


urlpatterns = [
    path('admin/', admin.site.urls),

    path('', login_view, name='login-view'),
    path('logout/', logout_view, name='logout-view'),
    path('dashboard/', dashboard, name='dashboard'),
    path('pos/', pos_view, name='pos_view'),
    path('sales/', sales, name='sales-view'),
    path('purchases/', purchases, name='purchases-view'),
    path('inventory/', inventory, name='inventory-view'),
    path('expenses/', expenses, name='expenses-view'),
    path('customers/', customer_view, name='customer-view'),
    path('edit_product/<str:id>/', edit_product, name='edit-product-view'),
    path('update_sale/<str:id>/', edit_sale, name='edit-sale-view'),
    path('undo_sale/<str:id>/', undo_sale, name='undo-sale-view'),



    path('reset_password/', auth_views.PasswordResetView.as_view(template_name = "recoverpw-1.html"), name='reset_password'),
    path('reset_password_sent/', auth_views.PasswordResetDoneView.as_view(template_name = "recoverpw_sent.html"), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name = "recoverpw_set.html"), name='password_reset_confirm'),
    path('reset_password_complete/', auth_views.PasswordResetCompleteView.as_view(template_name = "recoverpw_complete.html"), name='password_reset_complete'),


    
    path('api/dash_line_data/', DashboardLineData.as_view(), name='dashboard-bar-data'),
    path('api/dash_bar_data/', DashboardBarData.as_view(), name='dashboard-bar-data'),
    path('api/monthly_sales_data/', MonthlySalesLineData.as_view(), name='monthly-sales-data'),
    path('api/monthly_purchases_data/', MonthlyPurchasesLineData.as_view(), name='monthly-purchases-data'),
    path('api/monthly_expenses_data/', MonthlyExpensesLineData.as_view(), name='monthly-expenses-data'),
    path('api/daily_analysis_data/', DailyAnalysisData.as_view(), name='daily-analysis-data'),



    path('api/pos_product_data/', AllProducts.as_view(), name='pos-product-data'),
    path('api/pos/complete_sale/', complete_sale, name='complete-sale'),

]
