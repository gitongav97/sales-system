$(function() {
  'use strict'

  var gridLineColor = 'rgba(77, 138, 240, .1)';

  var colors = {
    primary:         "#727cf5",
    secondary:       "#7987a1",
    success:         "#42b72a",
    info:            "#68afff",
    warning:         "#fbbc06",
    danger:          "#ff3366",
    light:           "#ececec",
    dark:            "#282f3a",
    muted:           "#686868"
  }


  // Dashbaord date start
  if($('#dashboardDate').length) {
    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('#dashboardDate').datepicker({
      format: "dd-MM-yyyy",
      todayHighlight: true,
      autoclose: true
    });
    $('#dashboardDate').datepicker('setDate', today);
  }
  // Dashbaord date end


  // Progressgar1 start
  if($('#progressbar1').length) {

    var target = $('#progress_achieved').val();
    var progress = $('#progress_target').val();
    console.log(progress,target)
    

    var bar = new ProgressBar.Circle(progressbar1, {
      color: colors.primary,
      trailColor: gridLineColor,
      // This has to be the same size as the maximum width to
      // prevent clipping
      strokeWidth: 4,
      trailWidth: 1,
      easing: 'easeInOut',
      duration: 1400,
      text: {
        autoStyleContainer: false
      },
      from: { color: colors.primary, width: 1 },
      to: { color: colors.primary, width: 4 },
      // Set default step function for all animate calls
      step: function(state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        
    
        var value = Math.round(circle.value(progress/target) * 100);
        if (value === 0) {
          circle.setText('');
        } else {
          circle.setText(value + '%');
        }
    
      }
    });
    bar.text.style.fontFamily = "'Overpass', sans-serif;";
    bar.text.style.fontSize = '3rem';
    
    bar.animate();
  }
  // Progressgar1 start

  // Monthly sales chart start
  // if($('#monthly-sales-chart').length) {
  //   var monthlySalesChart = document.getElementById('monthly-sales-chart').getContext('2d');
  //     new Chart(monthlySalesChart, {
  //       type: 'bar',
  //       data: {
  //         labels: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
  //         datasets: [{
  //           label: 'Sales',
  //           data: [150,110,90,115,125,160,190,140,100,110,120,120],
  //           backgroundColor: colors.primary
  //         }]
  //       },
  //       options: {
  //         maintainAspectRatio: false,
  //         legend: {
  //           display: false,
  //             labels: {
  //               display: false
  //             }
  //         },
  //         scales: {
  //           xAxes: [{
  //             display: true,
  //             barPercentage: .3,
  //             categoryPercentage: .6,
  //             gridLines: {
  //               display: false
  //             },
  //             ticks: {
  //               fontColor: '#8392a5',
  //               fontSize: 10
  //             }
  //           }],
  //           yAxes: [{
  //             gridLines: {
  //               color: gridLineColor
  //             },
  //             ticks: {
  //               fontColor: '#8392a5',
  //               fontSize: 10,
  //               min: 80,
  //               max: 200
  //             }
  //           }]
  //         }
  //       }
  //     }
  //   );
  // }
  // Monthly sales chart end

});