$(function() {
  'use strict';

  $(function() {

    $.fn.dataTable.moment( 'MMM D, YYYY h:mm A' );

    $('#dataTableExample').DataTable({
      "aLengthMenu": [
        [10, 20,50, -1],
        [10, 20, 50, "All"]
      ],
      
      "iDisplayLength": 10,
      "language": {
        search: ""
      },
      
      "order": [[ 0, "desc" ]],
      
    });
    $('#dataTable1').DataTable({
      "aLengthMenu": [
        [10, 20,50, -1],
        [10, 20, 50, "All"]
      ],
      "iDisplayLength": 10,
      "language": {
        search: ""
      },
      columnDefs: [ { type: 'date', 'targets': [3] } ],
      "order": [[ 0, "desc" ]],
      
    });
    $('#dataTableExample','#dataTable1').each(function() {
      var datatable = $(this);
      // SEARCH - Add the placeholder for Search and Turn this into in-line form control
      var search_input = datatable.closest('.dataTables_wrapper').find('div[id$=_filter] input');
      search_input.attr('placeholder', 'Search');
      search_input.removeClass('form-control-sm');
      // LENGTH - Inline-Form control
      var length_sel = datatable.closest('.dataTables_wrapper').find('div[id$=_length] select');
      length_sel.removeClass('form-control-sm');
    });




  });

});