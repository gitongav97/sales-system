from django import forms
from django.db.models.base import Model
from django.forms import ModelForm
from .models import *
from django.db.models import Q
from django.contrib.auth import get_user_model



User = get_user_model()


class ProductForm(ModelForm):

    class Meta:
        model = Product
        fields = ('product_name','selling_price')
        widgets = {

            'product_name': forms.TextInput(attrs={'class': 'form-control'}),
            'selling_price': forms.NumberInput(attrs={'class': 'form-control'}),
        },




class PurchaseForm(ModelForm):

    class Meta:
        model = Purchase
        fields = ('product','quantity','buying_price')
        widgets = {

            'product': forms.TextInput(attrs={'class': 'form-control'}),
            'quantity': forms.NumberInput(attrs={'class': 'form-control'}),
            'buying_price': forms.NumberInput(attrs={'class': 'form-control'}),
        },

    

class SaleForm(ModelForm):

    class Meta:
        model = Sale
        fields = ('customer_name',)
        



class DamageForm(ModelForm):

    class Meta:
        model = Purchase
        fields = ('product','quantity')
        widgets = {

            'product': forms.TextInput(attrs={'class': 'form-control'}),
            'quantity': forms.NumberInput(attrs={'class': 'form-control'}),
        },



class EditProductForm(ModelForm):

    class Meta:
        model = Product
        fields = ('product_name','selling_price')
        widgets = {

            'product_name': forms.TextInput(attrs={'class': 'form-control'}),
            'selling_price': forms.NumberInput(attrs={'class': 'form-control'}),
        },



class TargetForm(ModelForm):

    class Meta:
        model = MonthlyTotalSales
        fields = ('sales_target',)
        widgets = {

            'sales_target': forms.NumberInput(attrs={'class': 'form-control'}),
        },



class UpdateSaleForm(ModelForm):

    class Meta:
        model = Payment
        fields = ('payment_choice', 'amount', 'payment_id')
        widgets = {

            'payment_choice':forms.TextInput(attrs={'class': 'form-control'}),
            'amount': forms.NumberInput(attrs={'class': 'form-control'}),
            'payment_id':forms.TextInput(attrs={'class': 'form-control'}),
        },
       


class UserLoginForm(forms.Form):
	
	query = forms.CharField(label='Username / Email', widget=forms.TextInput(attrs={'class': 'form-control'}))
	password = forms.CharField(label='Password', widget=forms.PasswordInput(attrs={'class': 'form-control'}))

	def clean(self, *args, **kwargs):
		
		query = self.cleaned_data.get('query')
		password = self.cleaned_data.get('password')
		user_qs_final = User.objects.filter(
                    Q(username__iexact=query) |
                    Q(email__iexact=query)
                ).distinct()
		

		if not user_qs_final.exists() and user_qs_final.count != 1:
			raise forms.ValidationError("Invalid credentials - user does not exist")
		
		user_obj = user_qs_final.first()
		
		if not user_obj.check_password(password):
			raise forms.ValidationError("Invalid credentials - user does not exist")
			
		self.cleaned_data["user_obj"] = user_obj
		
		return super(UserLoginForm, self).clean(*args, **kwargs)


class CustomerForm(ModelForm):

    class Meta:
        model = Customer
        fields = ('name','contact')
        widgets = {

            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'contact': forms.TextInput(attrs={'class': 'form-control'}),
        },



class ExpensesForm(ModelForm):

   class Meta:
      model = Expense
      fields = '__all__'
      widgets = {
         
         'date': forms.DateInput(attrs={'class':'form-control', 'placeholder':'Select a date', 'type':'date'}),
         'description': forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Description'}),
         'amount': forms.NumberInput(attrs={'class': 'form-control'}),

      }