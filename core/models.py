from django.db import models
from .models import *
from django.db.models import Sum

import datetime


# Create your models here.


class Product(models.Model):

    product_name = models.CharField(max_length=50, unique=True)
    quantity_in_stock = models.IntegerField(default=0)
    average_buying_price = models.IntegerField(default=0, null=True)
    selling_price = models.IntegerField(default=0)
    sale_counter =  models.IntegerField(default=0)
    purchase_counter =  models.IntegerField(default=0)

    def __str__(self):

        return '%s' % self.product_name

    def selling_stock_value(self):

        selling_stock_value = self.selling_price * self.quantity_in_stock
        return selling_stock_value

    
    def buying_stock_value(self):

        buying_stock_value = self.average_buying_price * self.quantity_in_stock
        return buying_stock_value



class Purchase(models.Model):

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    buying_price = models.IntegerField()
    total_cost = models.IntegerField()
    date_bought = models.DateTimeField(auto_now_add=True)

    def __str__(self):

        return '%s, %s, %s, %s' % (self.product, self.quantity, self.buying_price, self.date_bought)


class Customer(models.Model):

    name = models.CharField(max_length=50, unique=True)
    contact = models.CharField(max_length=50, null=True)


    def __str__(self):

        return '%s, %s' % (self.name, self.contact)


class Sale(models.Model):

    S_CHOICES = [
        ('PENDING', 'PENDING'),
        ('PAID', 'PAID'),
    ]


    products = models.JSONField(null=True, blank=True)
    total_cost = models.IntegerField(default=0)
    discount = models.IntegerField(default=0)
    actual_cost = models.IntegerField(default=0)
    date_sold = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=10, choices=S_CHOICES, default='PENDING')
    customer_name = models.ForeignKey(Customer, on_delete=models.CASCADE, null=True, blank=True)
    paid_amount = models.IntegerField()

    def __str__(self):

        return '%s, %s, %s' % (self.id, self.actual_cost, self.status)


    def balance(self):

        unpaid_balance = self.actual_cost - self.paid_amount
        return unpaid_balance


class MonthlyTotalSales(models.Model):

    month_and_year = models.DateField(auto_now_add=True)
    total_amount = models.IntegerField(default=0)
    profit = models.IntegerField(default=0)
    sales_target = models.IntegerField(default=0)

    def __str__(self):

        return '%s, %s' % (self.month_and_year, self.total_amount)


class MonthlyTotalPurchases(models.Model):

    month_and_year = models.DateField(auto_now_add=True)
    total_amount = models.IntegerField(default=0)

    def __str__(self):

        return '%s, %s' % (self.month_and_year, self.total_amount)


class DailySale(models.Model):

    date = models.DateField(auto_now_add=True)
    total_amount = models.FloatField(default=0)
    profit = models.FloatField(default=0)

    def __str__(self):

        return '%s, %s' % (self.date, self.total_amount)


class Damage(models.Model):

    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    damage_value = models.IntegerField()
    damage_date = models.DateTimeField(auto_now_add=True)


class MonthlyTotalDamage(models.Model):

    month_and_year = models.DateField(auto_now_add=True)
    total_amount = models.IntegerField(default=0)

    def __str__(self):

        return '%s, %s' % (self.month_and_year, self.total_amount)


class Payment(models.Model):
    
    P_CHOICES = [
        ('M-PESA', 'M_PESA'),
        ('CASH', 'CASH'),
        ('EQUITY', 'EQUITY')
    ]

    sale_id = models.ForeignKey(Sale, on_delete=models.CASCADE, null=True, blank=True)
    payment_id = models.CharField(max_length=50, null=True, blank=True, default="none")
    payment_choice = models.CharField(max_length=10, choices=P_CHOICES, default='CASH')
    amount = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):

        return '%s, %s, %s' % (self.date, self.payment_choice, self.amount)


class Expense(models.Model):

    date = models.DateField()
    description = models.CharField(max_length=100)
    amount = models.PositiveIntegerField()
    action_date = models.DateField(auto_now_add=True)

    def __str__(self):

        return '%s, %s, %s' % (self.description, self.amount, self.date)
    


class MonthlyTotalExpense(models.Model):

    month_and_year = models.DateField(auto_now_add=True)
    total_amount = models.IntegerField(default=0)

    def __str__(self):

        return '%s, %s' % (self.month_and_year, self.total_amount)