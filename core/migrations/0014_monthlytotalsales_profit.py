# Generated by Django 3.2.9 on 2021-11-11 12:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0013_alter_monthlytotalsales_month_and_year'),
    ]

    operations = [
        migrations.AddField(
            model_name='monthlytotalsales',
            name='profit',
            field=models.IntegerField(default=0),
        ),
    ]
