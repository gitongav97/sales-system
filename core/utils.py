
from core.serializers import *
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib import messages

from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from .models import *
import calendar
from datetime import datetime,date
from dateutil.relativedelta import relativedelta
import operator
import json


today = date.today()
yesterday = today - relativedelta(days=1)

current_month = datetime.now().month
current_year = datetime.now().year

previous_date = today - relativedelta(months=1)
previous_month = previous_date.month
previous_year = previous_date.year



class DashboardLineData(APIView):

    

    def get(self, request, format=None):

        daily_label = []
        daily_sale = []

        daily_sales = DailySale.objects.all().order_by('date')[:90]

        for sale in daily_sales:

            daily_label.append(sale.date)
            daily_sale.append(sale.total_amount)
        
        dashboard_line_data = {
            'daily_label':daily_label,
            'daily_sale':daily_sale
        }

        return JsonResponse(dashboard_line_data)



class DashboardBarData(APIView):


    def get(self, request, format=None):


        month_label = []
        month_purchase = []
        month_sale = []
        month_profit = []

        monthly_sales = MonthlyTotalSales.objects.all().order_by('month_and_year')[:6]

        for sale in monthly_sales:

            month = sale.month_and_year.month
            month_name = calendar.month_name[month]
            year = sale.month_and_year.year

            try:
                related_month_purchase = MonthlyTotalPurchases.objects.get(month_and_year__year=year, month_and_year__month=month)
                related_month_purchase =related_month_purchase.total_amount
            except:
                related_month_purchase=0

            month_label.append(str(month_name)+str(year))
            month_purchase.append(related_month_purchase)
            month_sale.append(sale.total_amount)
            month_profit.append(sale.profit)
            

        
        sale_month = datetime.now().month
        sale_year = datetime.now().year
        monthly_sales = MonthlyTotalSales.objects.get(month_and_year__month=sale_month, month_and_year__year=sale_year)
        target = monthly_sales.sales_target
        progress = monthly_sales.total_amount

        dashboard_bar_data = {
            'monthly_label':month_label,
            'monthly_purchase':month_purchase,
            'monthly_sale':month_sale,
            'monthly_profit':month_profit,
            'target':target,
            'progress':progress
            
        }

        return JsonResponse(dashboard_bar_data)



def sales_daily_percentage_difference(request):

    try:
        current_sales = DailySale.objects.get(date=today)
        yesterday_sales = DailySale.objects.get(date=yesterday)
    except:
        current_sales = 0
        yesterday_sales = 0


    daily_percent_change = 0
    try:
        daily_percent_change = round(((current_sales-yesterday_sales)/yesterday_sales) * 100,1)
    except:
        daily_percent_change = "N/A"

    return daily_percent_change


def sales_monthy_percentage_difference(request):

    try:
        current_month_sales = MonthlyTotalSales.objects.get(month_and_year__month=current_month, month_and_year__year=current_year)
        previous_month_sales = MonthlyTotalSales.objects.get(month_and_year__month=previous_month, month_and_year__year=previous_year)
    except:
        current_month_sales = 0
        previous_month_sales = 0


    monthly_sales_percent_change = 0
    try:
        monthly_sales_percent_change = round(((current_month_sales.total_amount - previous_month_sales.total_amount)/previous_month_sales.total_amount) * 100, 1)
    except:
        monthly_sales_percent_change = "N/A"


    return monthly_sales_percent_change



def profit_monthy_percentage_difference(request):

    try:
        current_month_sales = MonthlyTotalSales.objects.get(month_and_year__month=current_month, month_and_year__year=current_year)
        previous_month_sales = MonthlyTotalSales.objects.get(month_and_year__month=previous_month, month_and_year__year=previous_year)
    except:
        current_month_sales = 0
        previous_month_sales = 0



    monthly_profit_percent_change = 0
    try:
        monthly_profit_percent_change = round(((current_month_sales.profit - previous_month_sales.profit)/previous_month_sales.profit) * 100, 1)
    except:
        monthly_profit_percent_change = "N/A"



    return monthly_profit_percent_change


def most_sold_product(request):

    try:
        products = Product.objects.all().order_by('-sale_counter')
        product = products[0]
    except:
        product = None

    return product


def most_purchased_product(request):

    try:
        products = Product.objects.all().order_by('-purchase_counter')
        product = products[0]
    except:
        product = None

    return product


def purchases_monthy_percentage_difference(request):

    try:
        current_month_purchases = MonthlyTotalPurchases.objects.get(month_and_year__month=current_month, month_and_year__year=current_year)
        previous_month_purchases = MonthlyTotalPurchases.objects.get(month_and_year__month=previous_month, month_and_year__year=previous_year)
    except:
        current_month_purchases = 0
        previous_month_purchases = 0


    monthly_purchases_percent_change = 0
    try:
        monthly_purchases_percent_change = round(((current_month_purchases.total_amount - previous_month_purchases.total_amount)/previous_month_purchases.total_amount) * 100, 1)
    except:
        monthly_purchases_percent_change = "N/A"


    return monthly_purchases_percent_change


def expenses_monthy_percentage_difference(request):

    try:
        current_month_expenses = MonthlyTotalExpense.objects.get(month_and_year__month=current_month, month_and_year__year=current_year)
        previous_month_expenses = MonthlyTotalExpense.objects.get(month_and_year__month=previous_month, month_and_year__year=previous_year)
    except:
        current_month_expenses = 0
        previous_month_expenses = 0


    monthly_expenses_percent_change = 0
    try:
        monthly_expenses_percent_change = round(((current_month_expenses.total_amount - previous_month_expenses.total_amount)/previous_month_expenses.total_amount) * 100, 1)
    except:
        monthly_expenses_percent_change = "N/A"


    return monthly_expenses_percent_change


class DailyAnalysisData(APIView):


    def get(self, request, format=None):


        daily_label = []
        daily_sale = []
        daily_profit = []

        daily_sales = DailySale.objects.all().order_by('-date')[:30][::-1]

        for sale in daily_sales:

            daily_label.append(sale.date)
            daily_sale.append(sale.total_amount)
            daily_profit.append(sale.profit)
            

        

        daily_analysis_data = {
            
            'daily_label':daily_label,
            'daily_sale':daily_sale,
            'daily_profit':daily_profit,
            
        }

        return JsonResponse(daily_analysis_data)


class MonthlySalesLineData(APIView):
    

    def get(self, request, format=None):

        monthly_label = []
        monthly_sale = []
        monthly_profit = []

        monthly_sales = MonthlyTotalSales.objects.all().order_by('month_and_year')[:24]

        for sale in monthly_sales:

            month = calendar.month_name[sale.month_and_year.month]
            year = sale.month_and_year.year
            date = (month,year)

            monthly_label.append(date)
            monthly_sale.append(sale.total_amount)
            monthly_profit.append(sale.profit)
        
        monthly_sales_line_data = {
            'monthly_label':monthly_label,
            'monthly_sale':monthly_sale,
            'monthly_profit':monthly_profit
        }

        return JsonResponse(monthly_sales_line_data)


class MonthlyPurchasesLineData(APIView):


    def get(self, request, format=None):


        month_label = []
        month_purchase = []

        monthly_purchases = MonthlyTotalPurchases.objects.all().order_by('month_and_year')[:24]

        for sale in monthly_purchases:

            month = sale.month_and_year.month
            month_name = calendar.month_name[month]
            year = sale.month_and_year.year

            month_label.append(str(month_name)+str(year))
            month_purchase.append(sale.total_amount)            


        monthly_purchase_data = {
            'monthly_label':month_label,
            'monthly_purchase':month_purchase,
            
        }

        return JsonResponse(monthly_purchase_data)


class MonthlyExpensesLineData(APIView):


    def get(self, request, format=None):


        month_label = []
        month_expense = []

        monthly_expenses = MonthlyTotalExpense.objects.all().order_by('month_and_year')[:24]

        for sale in monthly_expenses:

            month = sale.month_and_year.month
            month_name = calendar.month_name[month]
            year = sale.month_and_year.year

            month_label.append(str(month_name)+str(year))
            month_expense.append(sale.total_amount)            


        monthly_expenses_data = {
            'monthly_label':month_label,
            'monthly_expense':month_expense,
            
        }

        return JsonResponse(monthly_expenses_data)


class AllProducts(APIView):

    def get(self, request):

        all_products = Product.objects.all()
        all_products = ProductSerializer(all_products, many=True)
        all_customers = Customer.objects.all()
        all_customers = CustomerSerializer(all_customers, many=True)

        return Response({"all_products":all_products.data, "all_customers":all_customers.data})


def complete_sale(request):

    today = date.today()
    sale_month = datetime.now().month
    sale_year = datetime.now().year

    data = request.body
    data = json.loads(data)
    products = data['Products']
    customer = Customer.objects.get(id=data['customer'])
    actual_cost = data['actual_cost']
    discount = data['discount']
    total_cost = data['total_cost']
    payment_method = data['payment_method']
    payment_id = data["payment_id"]
    paid = data['paid']

    if paid > total_cost:
        return JsonResponse({"error":"The paid amount is higher that the total amount. Please adjust"})

    
    try:
        instance = Sale.objects.create(products=products, total_cost=total_cost, discount=discount, actual_cost=actual_cost, paid_amount=paid, customer_name=customer)
        payment = Payment.objects.create(sale_id=instance, payment_choice=payment_method, amount=paid, payment_id=payment_id)
        payment.save()
        if actual_cost == paid:
            instance.status = 'PAID'
        else:
            instance.status = 'PENDING'

        instance.save()

    except:
        return JsonResponse({"error":"An error occured. Ensure the data you entered is correct"})

    profit = 0
    for product in products:

        product_id = product['product_id']
        quantity = product['quantity']
        sold_product_update = Product.objects.get(id=product_id)
        sold_product_update.quantity_in_stock = sold_product_update.quantity_in_stock - quantity
        sold_product_update.sale_counter = sold_product_update.sale_counter + 1
        profit = profit + ((sold_product_update.selling_price - sold_product_update.average_buying_price)*quantity)
        sold_product_update.save()
    
    # add to daily sale #
    try:
        sales_today = DailySale.objects.get(date=today)
        if sales_today:
            sales_today.total_amount = sales_today.total_amount + actual_cost
            sales_today.profit = sales_today.profit + (profit - discount)
            sales_today.save()

    except:
        daily_count = DailySale.objects.create(total_amount=actual_cost, profit=(profit - discount))
        daily_count.save()

    # add to monthly sales #
    try:
        monthly_sales = MonthlyTotalSales.objects.get(month_and_year__month=sale_month, month_and_year__year=sale_year)
        if monthly_sales:
            monthly_sales.total_amount = monthly_sales.total_amount + actual_cost
            monthly_sales.profit = monthly_sales.profit + (profit - discount)
            monthly_sales.save()

    except:
        monthly_sales = MonthlyTotalSales.objects.create(total_amount=actual_cost, profit=(profit - discount))
        monthly_sales.save()

    messages.success(request, 'Sale Completed Successfully')

    return JsonResponse({"success":"sale completed"})