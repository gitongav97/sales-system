from django.shortcuts import render, redirect
from .forms import *
from .models import *
from .utils import *

from django.contrib import messages
from datetime import date, datetime
import calendar
from django.db.models import Sum, ExpressionWrapper, F, IntegerField

from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout

# Create your views here.


@login_required(login_url='login-view')
def dashboard(request):

    today = date.today()
    purchase_month = datetime.now().month
    purchase_year = datetime.now().year
    purchase_month_name = calendar.month_name[purchase_month]
    purchase_form = PurchaseForm(request.POST or None)
    add_product_form = ProductForm(request.POST or None)
    
    try:
        monthly_sales = MonthlyTotalSales.objects.get(month_and_year__month=purchase_month, month_and_year__year=purchase_year)
    except:
        monthly_sales = MonthlyTotalSales.objects.create(month_and_year=today)

    target_form = TargetForm(request.POST or None, instance=monthly_sales)

    # make new purchase logic #
    if purchase_form.is_valid():

        # create new purchase in DB #
        product_purchased = purchase_form.cleaned_data.get('product')
        purchased_quantity = purchase_form.cleaned_data.get('quantity')
        purchase_price = purchase_form.cleaned_data.get('buying_price')

        instance = Purchase.objects.create(product=product_purchased, quantity=purchased_quantity, buying_price=purchase_price, total_cost=(purchased_quantity * purchase_price))
        instance.save()

        #  add total cost to monthly purchase total   #
        try:
            monthly_purchase = MonthlyTotalPurchases.objects.get(month_and_year__month=purchase_month, month_and_year__year=purchase_year)
            if monthly_purchase:
                monthly_purchase.total_amount = monthly_purchase.total_amount + (purchased_quantity * purchase_price)
                monthly_purchase.save()

        except:
            monthly_purchase = MonthlyTotalPurchases.objects.create(total_amount = (purchased_quantity * purchase_price))
            monthly_purchase.save()


        # update product quantity and average buying price in DB #
        product_to_update = Product.objects.get(id=product_purchased.id)
        if product_to_update.average_buying_price is 0:
            product_to_update.average_buying_price = purchase_price
        else:
            product_to_update.average_buying_price = ((purchase_price * purchased_quantity) + (product_to_update.average_buying_price * product_to_update.quantity_in_stock))/(purchased_quantity + product_to_update.quantity_in_stock)

        product_to_update.quantity_in_stock = purchased_quantity + product_to_update.quantity_in_stock
        product_to_update.save()

        messages.success(request, 'Purchase Added Successfully')

        return redirect ("purchases-view")


    # add new product logic #
    elif add_product_form.is_valid():
        
        add_product_form.save(commit=True)
        messages.success(request, 'Product Added Successfully')

        return redirect ("dashboard")


    # update sales target #
    elif target_form.is_valid():
        target_form.save()

        messages.success(request, 'Target Updated Successfully')
        return redirect('dashboard')



    #get daily sales total #
    try:
        sales_today = DailySale.objects.get(date=today)
    except:
        sales_today = DailySale.objects.create(date=today)


    # get daily purchases total #
    try:
        daily_purchase = Purchase.objects.filter(date_bought__date=today).aggregate(Sum('total_cost'))
        daily_purchase = daily_purchase.get("total_cost__sum")
    except:
        daily_purchase = 0


    # monthly sales target #
    try:
        monthly_target = MonthlyTotalSales.objects.get(month_and_year__year=purchase_year, month_and_year__month=purchase_month)
    except:
        monthly_target = MonthlyTotalSales.objects.create(month_and_year=today, sales_target=0)



    unpaid_sales = Sale.objects.filter(status='PENDING')
    try:
        unpaid_sales_total_cost = Sale.objects.filter(status='PENDING').aggregate(Sum('total_cost')).get('total_cost__sum')
        unpaid_sales_paid_amount = Sale.objects.filter(status='PENDING').aggregate(Sum('paid_amount')).get('paid_amount__sum')
        unpaid_sales_total = unpaid_sales_total_cost - unpaid_sales_paid_amount
    except:
        unpaid_sales_total_cost = 0
        unpaid_sales_paid_amount = 0
        unpaid_sales_total = 0


    daily_percent_change = sales_daily_percentage_difference(request)
    monthly_percent_change = profit_monthy_percentage_difference(request)

    context = {

        'purchase_form':purchase_form,
        'add_product_form':add_product_form,
        'sales_today':sales_today,
        'daily_purchase':daily_purchase,
        'monthly_target':monthly_target,
        'monthly_sales':monthly_sales,
        'target_form':target_form,
        'unpaid_sales':unpaid_sales,
        'unpaid_sales_total':unpaid_sales_total,
        'purchase_month_name':purchase_month_name,
        'daily_percent_change':daily_percent_change,
        'monthly_percent_change':monthly_percent_change,

    }  


    return render(request, 'dashboard.html', context)


@login_required(login_url='login-view')
def pos_view(request):

    form = SaleForm()      

    context = {

        'form':form
    }
    
    return render (request, "pos.html", context)


@login_required(login_url='login-view')
def sales(request):

    today = date.today()
    sale_month = datetime.now().month
    sale_year = datetime.now().year
    sale_form = SaleForm(request.POST or None)
    sales_month_name = calendar.month_name[sale_month]


    all_sales =Sale.objects.all()
    daily_percent_change = sales_daily_percentage_difference(request)
    monthly_percent_change = sales_monthy_percentage_difference(request)
    most_sold = most_sold_product(request)

    #get daily sales total #
    try:
        sales_today = DailySale.objects.get(date=today)
    except:
        sales_today = DailySale.objects.create(date=today)


    # get current month sales #
    try:
        current_month_sales = MonthlyTotalSales.objects.get(month_and_year__month=sale_month, month_and_year__year=sale_year)
    except:
        current_month_sales = MonthlyTotalSales.objects.create()


    context = {

        'sale_form':sale_form,
        'all_sales':all_sales,
        'sales_today':sales_today,
        'current_month_sales':current_month_sales,
        'sales_month_name':sales_month_name,
        'daily_percent_change':daily_percent_change,
        'monthly_percent_change':monthly_percent_change,
        'most_sold':most_sold

    }

    return render(request, 'sales.html', context)


@login_required(login_url='login-view')
def purchases(request):

    purchase_month = datetime.now().month
    purchase_year = datetime.now().year
    purchase_month_name = calendar.month_name[purchase_month]


    purchase_form = PurchaseForm(request.POST or None)
    if purchase_form.is_valid():

        # create new purchase in DB #
        product_purchased = purchase_form.cleaned_data.get('product')
        purchased_quantity = purchase_form.cleaned_data.get('quantity')
        purchase_price = purchase_form.cleaned_data.get('buying_price')

        instance = Purchase.objects.create(product=product_purchased, quantity=purchased_quantity, buying_price=purchase_price, total_cost=(purchased_quantity * purchase_price))
        instance.save()
    
        prod=Product.objects.get(id=product_purchased.id)
        prod.purchase_counter = prod.purchase_counter + 1
        prod.save()

        #  add total cost to monthly purchase   #
        try:
            monthly_purchase = MonthlyTotalPurchases.objects.get(month_and_year__month=purchase_month, month_and_year__year=purchase_year)
            if monthly_purchase:
                monthly_purchase.total_amount = monthly_purchase.total_amount + (purchased_quantity * purchase_price)
                monthly_purchase.save()

        except:
            monthly_purchase = MonthlyTotalPurchases.objects.create(total_amount = (purchased_quantity * purchase_price))
            monthly_purchase.save()


        # update product quantity and average buying price in DB #
        product_to_update = Product.objects.get(id=product_purchased.id)
        if product_to_update.average_buying_price is 0:
            product_to_update.average_buying_price = purchase_price
        else:
            product_to_update.average_buying_price = ((purchase_price * purchased_quantity) + (product_to_update.average_buying_price * product_to_update.quantity_in_stock))/(purchased_quantity + product_to_update.quantity_in_stock)

        product_to_update.quantity_in_stock = purchased_quantity + product_to_update.quantity_in_stock
        product_to_update.save()

        messages.success(request, 'Purchase Added Successfully')

        return redirect ("purchases-view")

    most_purchased = most_purchased_product(request)
    monthly_percentage_change = purchases_monthy_percentage_difference(request)

    all_purchases = Purchase.objects.all()
    six_month_purchases = MonthlyTotalPurchases.objects.all()[:5].aggregate(Sum('total_amount')).get('total_amount__sum')
    try:
        monthly_purchase = MonthlyTotalPurchases.objects.get(month_and_year__month=purchase_month, month_and_year__year=purchase_year)
    except:
        monthly_purchase = MonthlyTotalPurchases.objects.create()



    


    context = {

        'purchase_form':purchase_form,
        'all_purchases':all_purchases,
        'monthly_purchase':monthly_purchase,
        'purchase_month_name':purchase_month_name,
        'six_month_purchases':six_month_purchases,
        'most_purchased':most_purchased,
        'monthly_percentage_change':monthly_percentage_change
        
        
    }

    return render(request, 'purchases.html', context)


@login_required(login_url='login-view')
def inventory(request):

    purchase_month = datetime.now().month
    purchase_year = datetime.now().year

    all_products = Product.objects.all()
    all_damages = Damage.objects.all()

    selling_value = all_products.annotate(value = ExpressionWrapper(F('selling_price')*F('quantity_in_stock'),output_field=IntegerField())).aggregate(selling_value=Sum("value"))['selling_value']
    buying_value = all_products.annotate(value = ExpressionWrapper(F('average_buying_price')*F('quantity_in_stock'),output_field=IntegerField())).aggregate(buying_value=Sum("value"))['buying_value']


    damage_form = DamageForm(request.POST or None)
    add_product_form = ProductForm(request.POST or None)
    if damage_form.is_valid():

        # create new damage in DB #
        product_damaged = damage_form.cleaned_data.get('product')
        damaged_quantity = damage_form.cleaned_data.get('quantity')
        

        instance = Damage.objects.create(product=product_damaged, quantity=damaged_quantity, damage_value=damaged_quantity * product_damaged.average_buying_price)
        instance.save()

        #  add total cost to monthly damage   #
        try:
            monthly_damage = MonthlyTotalDamage.objects.get(month_and_year__month=purchase_month, month_and_year__year=purchase_year)
            if monthly_damage:
                monthly_damage.total_amount = monthly_damage.total_amount + (damaged_quantity * product_damaged.average_buying_price)
                monthly_damage.save()

        except:
            monthly_damage = MonthlyTotalDamage.objects.create(total_amount = (damaged_quantity * product_damaged.average_buying_price))
            monthly_damage.save()


        # update product quantity and average buying price in DB #
        product_to_update = Product.objects.get(id=product_damaged.id)
        product_to_update.quantity_in_stock = product_to_update.quantity_in_stock - damaged_quantity
        product_to_update.save()

        messages.success(request, 'Damage Added Successfully')

        return redirect ("inventory-view")


    # add new product logic #
    elif add_product_form.is_valid():
        
        add_product_form.save(commit=True)
        messages.success(request, 'Product Added Successfully')
        return redirect ("inventory-view")

    context = {

        'buying_value':buying_value,
        'selling_value':selling_value,
        'all_products':all_products,
        'add_product_form':add_product_form,
        'damage_form':damage_form,
        'all_damages':all_damages

    }

    return render(request, 'inventory.html', context)


@login_required(login_url='login-view')
def customer_view(request):

    form =CustomerForm(request.POST or None)
    if form.is_valid():

        form.save()
        messages.success(request, 'Customer Registered Successfully')


    customers = Customer.objects.all()
    context = {

        "form":form,
        "customers":customers

    }

    return render (request, "customers.html", context)


@login_required(login_url='login-view')
def expenses(request):

    expense_month = datetime.now().month
    expense_year = datetime.now().year
    expense_month_name = calendar.month_name[expense_month]

    expense_form = ExpensesForm(request.POST or None)
    if expense_form.is_valid():

        # add expense to DB #
        date = expense_form.cleaned_data.get("date")
        description = expense_form.cleaned_data.get("description")
        amount = expense_form.cleaned_data.get("amount")


        instance = Expense.objects.create(date=date, description=description, amount=amount)
        instance.save()

        #  add total expense to monthly expense   #
        actual_month = date.month
        actual_year = date.year
        try:
            monthly_expense = MonthlyTotalExpense.objects.get(month_and_year__month=actual_month, month_and_year__year=actual_year)
            if monthly_expense:
                monthly_expense.total_amount = monthly_expense.total_amount + amount
                monthly_expense.save()

        except:
            monthly_expense = MonthlyTotalExpense.objects.create(total_amount = amount)
            monthly_expense.save()


        messages.success(request, 'Purchase Added Successfully')

        return redirect ("expenses-view")


    all_expenses = Expense.objects.all()
    monthly_percent_change = expenses_monthy_percentage_difference(request)
    six_month_expenses = MonthlyTotalExpense.objects.all()[:5].aggregate(Sum('total_amount')).get('total_amount__sum')
    try:
        monthly_expense = MonthlyTotalExpense.objects.get(month_and_year__month=expense_month, month_and_year__year=expense_year)
    except:
        monthly_expense = MonthlyTotalExpense.objects.create()


    context = {

        'form':expense_form,
        'expense_month_name':expense_month_name,
        'all_expenses':all_expenses,
        'monthly_expense':monthly_expense,
        'monthly_percent_change':monthly_percent_change,
        'six_months_expenses':six_month_expenses

    }

    return render(request, 'expenses.html', context)


@login_required(login_url='login-view')
def edit_product(request, id,*args, **kwargs):

    product = Product.objects.get(id=id)
    form = EditProductForm(request.POST or None,instance=product)
    if form.is_valid():

        form.save()
        messages.success(request, 'Product Updated Successfully')

        return redirect ('inventory-view')

    context= {

        'form':form,
    }

    return render (request, "edit.html", context)


@login_required(login_url='login-view')
def edit_sale(request, id,*args, **kwargs):

    sale = Sale.objects.get(id=id)
    balance = sale.balance()
    payments = Payment.objects.filter(sale_id=sale)
    form = UpdateSaleForm(request.POST or None)
    if form.is_valid():

        amount = form.cleaned_data.get('amount')
        payment_method = form.cleaned_data.get('payment_choice')

        if amount > balance:

            messages.error(request, 'The paid amount is higher that the total amount. Please adjust')
            return redirect ('edit-sale-view', sale.id)

        else:
            payment = Payment.objects.create(sale_id=sale, payment_choice=payment_method, amount=amount)
            payment.save()

            
            sale.paid_amount = sale.paid_amount + int(amount)

            if sale.actual_cost == sale.paid_amount:
                sale.status = 'PAID'

            sale.save()

            messages.success(request, 'Sale Updated Successfully')

            return redirect ('sales-view')

    context= {

        'sale':sale,
        'payments':payments,
        'form':form,
    }

    return render (request, "edit_sale.html", context)


@login_required(login_url='login-view')
def undo_sale(request, id,*args, **kwargs):

    sale = Sale.objects.get(id=id)
    sale_date= sale.date_sold.date()
    sale_month = sale.date_sold.month
    sale_year = sale.date_sold.year


    profit = 0
    for prod in sale.products:
        product_id = prod['product_id']
        quantity = prod['quantity']
        sold_product_update = Product.objects.get(id=product_id)
        sold_product_update.quantity_in_stock = sold_product_update.quantity_in_stock + quantity
        sold_product_update.sale_counter = sold_product_update.sale_counter - 1
        profit = profit + ((sold_product_update.selling_price - sold_product_update.average_buying_price)*quantity)
        sold_product_update.save()

    
    # subtract from daily sale #
    try:
        sale_day = DailySale.objects.get(date=sale_date)
        if sale_day:
            sale_day.total_amount = sale_day.total_amount - sale.actual_cost
            sale_day.profit = sale_day.profit - (profit+sale.discount)
            sale_day.save()

    except:
        messages.error(request, 'An error occured')

    
    # subtract from monthly sales #
    try:
        monthly_sales = MonthlyTotalSales.objects.get(month_and_year__month=sale_month, month_and_year__year=sale_year)
        if monthly_sales:
            monthly_sales.total_amount = monthly_sales.total_amount - sale.actual_cost
            monthly_sales.profit = monthly_sales.profit - (profit + sale.discount)
            monthly_sales.save()

    except:
        messages.error(request, 'An error occured')

    
    sale.delete()
    messages.success(request, 'Sale Deleted Successfully')

    return redirect('sales-view')


def login_view(request):
    
    form = UserLoginForm(request.POST or None)
    next = request.GET.get('next')

    if form.is_valid():
        user_obj = form.cleaned_data.get('user_obj')
        login(request, user_obj)
        messages.success(request, 'Log In Successfully')
        if next:
            return redirect(next)
        return redirect('dashboard')


    context = {
        "form": form,
    }

    return render(request, 'login.html', context)


def logout_view(request):

    logout(request)

    return redirect ("login-view")


