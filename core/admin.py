from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Product)
admin.site.register(Purchase)
admin.site.register(Sale)
admin.site.register(MonthlyTotalSales)
admin.site.register(MonthlyTotalPurchases)
admin.site.register(DailySale)
admin.site.register(Damage)
admin.site.register(MonthlyTotalDamage)
admin.site.register(Customer)
admin.site.register(Payment)
admin.site.register(Expense)
admin.site.register(MonthlyTotalExpense)
